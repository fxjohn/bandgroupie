import webapp2
import logging
import os
import json
import datetime
import time
import urllib
import re
import random
import jinja2
import cgi
import scrapper

from google.appengine.api import users
from google.appengine.api import mail
from google.appengine.ext import ndb
from google.appengine.api import urlfetch
from google.appengine.ext.webapp.util import login_required

#---------------------------------------
# GLOBALS
#---------------------------------------
HOST = 'http://%s' % os.environ['HTTP_HOST']
JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)
musicAPI = {
    'LASTFM' : {
        'k' : 'a1b0d664ac27b839a2a63215fbf24b43',
        'u' : 'http://ws.audioscrobbler.com/2.0/?method=artist.getsimilar&api_key=%s&artist=%s&format=json&limit=10'
                },
    'ECHONT' : {
        'k' : 'CE5R35YHRS2WO0EH2',
        'u' : 'http://developer.echonest.com/api/v4/artist/similar?api_key=%s&name=%s&format=json'
                }
            }
CHRONICLE_LINK = 'http://www.austinchronicle.com/gyrobase/m/Calendar/MusicListings?oid='
urlfetch.set_default_fetch_deadline(60)

#---------------------------------------
# BASIC FUNCTIONS
#---------------------------------------
def json_header_response(self):
    return self.response.headers.add_header('content-type', 'application/json', charset='utf-8')

def packJson(obj):
    return json.dumps(obj)

def get_user():
     u = users.get_current_user()
     return u.email()


def format_show(show_list):
    out = ''
    for l in show_list:
        out += (l+', ')
    out = re.sub(',\s*$', '', out)
    return out


def format_date(d_string):
    try:
        if re.search('[ap]m', d_string):
            t = time.strptime(d_string, "%Y-%m-%d %I:%M%p")
        else:
            t = time.strptime(d_string, "%Y-%m-%d %H:%M")

        d_nice = time.strftime("%Y.%m.%d %H:%M %a", t)
        return d_nice
    except:
        return d_string


def matchUser2Concerts(USER):
    user_obj = user.get_or_insert(USER)
    #GET CONCERT EID AND NAME
    c  = {}
    concert_query = concert.query()
    if concert_query is not None:
        for concert_obj in concert_query.fetch():
            eid = concert_obj.key
            temp_string = " ".join(concert_obj.name).lower()
            temp_string = re.sub('&amp;', '&', temp_string)
            c[eid] = temp_string
    
    #BOTH BAND AND C[EID] IN UNICODE FORMAT --> CHANGE TO STRING
    user_obj.concerts = []
    for band in user_obj.bands:
        for eid in c:
            bs = "%s" % band
            cs = "%s" % c[eid]
            if bs.lower() in cs:
                logging.info(bs+" -> "+cs)
                user_obj.concerts.append(eid)
                user_obj.status = 'freshman'
                continue
                
    user_obj.put()          
 
#---------------------------------------
# DATASTORE DEFINITIONS
#---------------------------------------
class user(ndb.Model):
    """Models an individual user entry."""
    name = ndb.StringProperty()
    email = ndb.StringProperty()
    status = ndb.StringProperty(indexed=False)
    bands = ndb.StringProperty(repeated=True)
    similar_bands = ndb.StringProperty(repeated=True)
    notification_frequency = ndb.IntegerProperty(default=0, indexed=False)
    date = ndb.DateTimeProperty(auto_now=True, indexed=False) #updates for every object update
    concerts = ndb.KeyProperty(repeated=True, indexed=False)
    suggested_concerts = ndb.KeyProperty(repeated=True, indexed=False)

class band(ndb.Model): #key is band name in lower case
    """Models an individual band/artist entry"""
    name = ndb.StringProperty()
    date = ndb.DateTimeProperty(auto_now=True, indexed=False) #updates for every object update
    similar_bands = ndb.StringProperty(repeated=True)
    query_string = ndb.StringProperty(indexed=False)
    #shows = ndb.KeyProperty(repeated=True)

class concert(ndb.Model):
    """Models an concert entry, band is ancestor"""
    #e_id = ndb.IntegerProperty() ###  USE AS KEY
    name = ndb.StringProperty(repeated=True) #lower case
    venue = ndb.StringProperty(indexed=False)
    added = ndb.DateTimeProperty(auto_now=True, indexed=False) #updates for every object update
    date = ndb.StringProperty(indexed=False)
    moreinfo = ndb.StringProperty(indexed=False)


#---------------------------------------
# SERVER INTERFACE HANDLERS DEFINITIONS
#---------------------------------------
class ConcertsInfo(webapp2.RequestHandler):
    def get(self):
        if users.get_current_user():
            USER = get_user()
        else:
           self.redirect('/')
           return

        #CHECK IF NEW USER
        user_obj = user.get_or_insert(USER)
        if not user_obj.status:

            #DONE? --  grab name from USER (email) and set only once
            user_obj.name = users.get_current_user().nickname()
            user_obj.email = USER
            user_obj.status = 'new'

            user_obj.put()
            self.redirect('/settings')
            return

        concerts = []
        suggestions = []

        #SHOW CONCERT INFO
        for c_key in user_obj.concerts:
            concert_obj = c_key.get()
            #logging.info("%s" % concert_obj)
            d = format_date(concert_obj.date)
            v = concert_obj.venue
            n = format_show(concert_obj.name)
            l = concert_obj.moreinfo
            k = concert_obj.key.id()
            concerts.append([d,v,n,l,k])

        for c_key in user_obj.suggested_concerts:
            concert_obj = c_key.get()
            #logging.info("%s" % concert_obj)
            d = format_date(concert_obj.date)
            v = concert_obj.venue
            n = format_show(concert_obj.name)
            l = concert_obj.moreinfo
            k = concert_obj.key.id()
            suggestions.append([d,v,n,l,k])


        template_values = {
            'concerts': sorted(concerts),
            'suggestions': sorted(suggestions),
            'user': user_obj.name, 
            'logout' : users.create_logout_url("/")
        }

        template = JINJA_ENVIRONMENT.get_template('concerts.html')
        self.response.write(template.render(template_values))

    def post(self):
        try:
            jresponse = json.loads(self.request.body)
            eid = int(jresponse['eid'])
            req_type = jresponse['type']
        except:
            self.error(400)
            return

        logging.info("c_req: %s %s" % (eid, req_type))

        USER = get_user()
        user_obj = user.get_by_id(USER)

        temp_list = []
        tband_list = []
        target_key = ''

        for c_key in user_obj.suggested_concerts:
            if not int(c_key.id()) == eid:
                temp_list.append(c_key)
            else:
                target_key = c_key

        user_obj.suggested_concerts = temp_list

        #TO DO
        #   -- from target concert, find band
        #   -- add to ignore_list (bands) in user ndb
        #   -- remove from suggested band list too
        #   -- amend suggester to honor ignore_list from user ndb
        if req_type == 'rm' and target_key:
            concert_obj = target_key.get()
            show = " ".join(concert_obj.name).lower()
            for b in user_obj.similar_bands:
                if not b.lower() in show:
                    tband_list.append(b)

            user_obj.similar_bands = tband_list
            logging.info("removed: %s %s" % (show, tband_list))

        user_obj.put()


class UserSettings(webapp2.RequestHandler):
    def get(self):
        if users.get_current_user():
           USER = get_user()
        else:
           self.redirect('/')
           return

        user_obj = user.get_or_insert(USER)

        template_values = {
            'user': user_obj.name,
            'bands': sorted(user_obj.bands),
            'e_freq': user_obj.notification_frequency
        }

        #logging.info("%s\t%s" % (user_obj.name, user_obj.bands))
        template = JINJA_ENVIRONMENT.get_template('settings.html')
        self.response.write(template.render(template_values))

    def post(self):
        try:
            jresponse = json.loads(self.request.body)
            bands = jresponse['bs']
            email_freq = jresponse['ef']
        except:
            self.error(400)
            return

        logging.info("%s\t%s" % (email_freq, bands))

        #ADD BANDS
        USER = get_user()
        user_obj = user.get_or_insert(USER)
        #user_key = user_obj.key

        #clear previously set bands if any
        user_obj.bands = []

        for band_name in bands:

            #band_name = cgi.escape(bn)
            #convert to query friendly string
            qs = re.sub("\s+", "+", band_name.lower())
            qs = re.sub("&", "and", qs)

            user_obj.bands.append(band_name)
            band_obj = band.get_or_insert(band_name.lower())
            band_obj.query_string = qs
            band_obj.name = band_name
            band_obj.put()

        user_obj.notification_frequency = int(email_freq)
        status = user_obj.status
        user_obj.put()
        
        if status == 'new':
            matchUser2Concerts(USER)
            
        return    


#---------------------------------------
# CRON JOBS DEFINITIONS
#---------------------------------------
class Scrapper(webapp2.RequestHandler): #SCRAPPER (run daily)
    def get(self):

        #CLEAR CONCERTS DB
        concert_query = concert.query()
        if concert_query is not None:
            for c_key in concert_query.fetch(keys_only=True):
                c_key.delete()

        #data_c3c[e_id] = { door_time, event_date, event_title, ticket_link_raw, venue_name }
        c3c_dict = scrapper.c3c()
        for eid in c3c_dict:
            concert_obj = concert.get_or_insert(eid)
            if concert_obj.date is None:
                c_dict = c3c_dict[eid]
                #logging.info("c: %s"%c_dict)

                concert_obj.name.append(c_dict['event_title'])
                concert_obj.venue =  c_dict['venue_name']
                concert_obj.date = "%s %s" % (c_dict['event_date'], c_dict['door_time'])
                concert_obj.moreinfo =  c_dict['ticket_link_raw']

                artist = concert_obj.name
                logging.info("artist: %s"%artist)

                concert_obj.put()

        #data[e_id] = [ 0=date, 1:(n-2)=artists, (n-1)=venue, n=time]
        chronicle_dict = scrapper.chronicle() #add function parameter num of days?
        for eid in chronicle_dict:
            concert_obj = concert.get_or_insert(eid)
            if concert_obj.date is None:
                c_list = chronicle_dict[eid]
                l = len(c_list)
                a_a = c_list[1:(l-2)]
                logging.info("artist(s): %s"%a_a)

                for a in a_a:
                    concert_obj.name.append(a)

                concert_obj.date = "%s %s" % (c_list[0], c_list[l-1])
                concert_obj.venue =  c_list[l-2]
                concert_obj.moreinfo = "%s%s" % (CHRONICLE_LINK,eid)

                concert_obj.put()

        logging.info('Scrapper Done!')


class Suggester(webapp2.RequestHandler): #SUGGESTER (run daily)
    def get(self):

        #CLEAR SUGGESTIONS
        user_query = user.query()
        if user_query is not None:
            for user_obj in user_query.fetch():
                user_obj.similar_bands = []
                user_obj.put()


        band_query = band.query()
        #HAD TO CREATE DICT BECAUSE OF LIST MUTATION WITH OBJ PUT()
        d1 = {}  #ECHONEST
        d2 = {}  #LASTFM

        for band_obj in band_query.fetch():

            if not band_obj.similar_bands:
                
                #echonest
                response = getSimilar('ECHONT', band_obj.query_string)
                try:
                    if not json.loads(response.content)['response']['status']['code']:
                        similar = json.loads(response.content)['response']['artists']
                        d1[band_obj.name] = similar
                except:
                    logging.info("ECHONT Search failed: %s"%band_obj.query_string)

                #lastfm
                response = getSimilar('LASTFM', band_obj.query_string)
                try:
                    similar = json.loads(response.content)['similarartists']['artist']
                    d2[band_obj.name] = similar
                except:
                    logging.info("LASTFM Search failed: %s"%band_obj.query_string)


                for b in d1:
                    #logging.info ( "%s : %s" % (b,d1[b]) )
                    for sim_band in d1[b]:
                        connectSuggestedBand(b, sim_band['name'])

                for b in d2:
                    #logging.info ( "%s : %s" % (b,d2[b]) )
                    if d2[b] is dict:
                        for sim_band in d2[b]:
                            connectSuggestedBand(b, sim_band['name'])

            else:
                user_query = user.query(user.bands==band_obj.name)
                if user_query is not None:
                    for user_obj in user_query.fetch():
                        for suggested_band in band_obj.similar_bands:
                            if not (suggested_band in user_obj.bands) or not (suggested_band in user_obj.similar_bands):
                                user_obj.similar_bands.append(suggested_band)
                                user_obj.put()


        #GET RID OF REPEATED ENTRIES
        user_query = user.query()
        if user_query is not None:
            for user_obj in user_query.fetch():
                temp_set = set (user_obj.similar_bands)
                user_obj.similar_bands = list(temp_set)
                user_obj.put()

        logging.info('Suggester Done!')


class Matcher(webapp2.RequestHandler): #MATCHER (run daily)
    def get(self):

        #CLEAR CONCERT INFO
        user_query = user.query()
        if user_query is not None:
            for user_obj in user_query.fetch():
                user_obj.concerts = []
                user_obj.suggested_concerts = []
                user_obj.put()

        #GET CONCERT EID AND NAME
        c  = {}
        concert_query = concert.query()
        if concert_query is not None:
            for concert_obj in concert_query.fetch():
                eid = concert_obj.key
                temp_string = " ".join(concert_obj.name).lower()
                temp_string = re.sub('&amp;', '&', temp_string)
                c[eid] = temp_string
                #if re.search('4414', "%s"%eid): logging.info("%s"%concert_obj.name)

        #PUT TOGETHER BANDS, CONCERTS AND USERS
        user_query = user.query()
        if user_query is not None:
            for user_obj in user_query.fetch():

                #BOTH BAND AND C[EID] IN UNICODE FORMAT --> CHANGE TO STRING
                for band in user_obj.bands:
                    #logging.info("band: '%s'" % ( band) )
                    for eid in c:
                        bs = "%s" % band
                        cs = "%s" % c[eid]
                        #DEBUG -- if re.search('4414', "%s"%eid): logging.info(bs+" ? "+cs)
                        if bs.lower() in cs:
                            logging.info(bs+" -> "+cs)
                            user_obj.concerts.append(eid)
                            continue

                for band in user_obj.similar_bands:
                    #logging.info("band: '%s'" % ( band) )
                    for eid in c:
                        bs = "%s" % band
                        cs = "%s" % c[eid]
                        #rgx = "^\s*%s\s
                        if bs.lower() in cs:
                            logging.info(bs+" -> "+cs)
                            user_obj.suggested_concerts.append(eid)
                            continue

                user_obj.put()

        logging.info('Matcher Done!')


class Mailer(webapp2.RequestHandler): #MAILER (run daily ... mod user freq with day of year)
    def get(self):
        
        day_of_year = datetime.datetime.now().timetuple().tm_yday
        
        user_query = user.query()
        if user_query is not None:
            for user_obj in user_query.fetch():
                if user_obj.notification_frequency:
                    if day_of_year%user_obj.notification_frequency == 0:
                        email = user_obj.email
                        fav = user_obj.concerts
                        sug = user_obj.suggested_concerts
                        
                        if (fav or sug) and email:
                            send_mail(email, fav, sug)
                        

class login(webapp2.RequestHandler):
    def get(self):
        if users.get_current_user():
            self.redirect('/concerts')
        else:
            self.redirect(users.create_login_url(self.request.path))

#---------------------------------------
# CRON JOB FUNCTIONS
#---------------------------------------
def send_mail(email, fav, sug):
    
    #BUILD FLYER
    flyer ='Hey Friend!\nHere are some concerts and shows we found for you:'
    
    if fav:
        bulk = ''
        for c_key in fav:
            concert_obj = c_key.get()
            d = format_date(concert_obj.date)
            v = concert_obj.venue
            n = format_show(concert_obj.name)
            l = concert_obj.moreinfo
            
            bulk += "\n%s\n%s @ %s\n%s\n" % (n,d,v,l)

        flyer += "\n\nFROM YOUR FAVORITE BANDS\n"
        flyer += bulk 
    
    if sug:
        bulk = ''
        for c_key in sug:
            concert_obj = c_key.get()
            d = format_date(concert_obj.date)
            v = concert_obj.venue
            n = format_show(concert_obj.name)
            l = concert_obj.moreinfo
            
            bulk += "\n%s\n%s @ %s\n%s\n" % (n,d,v,l)

        flyer += "\n\nSUGGESTIONS BASED ON YOUR FAVORITE BANDS\n"
        flyer += bulk 
    
    flyer += "\n\nDon't forget to visit us to remove or add more bands, or change your settings!\n"
    flyer += "Cheers.\nJohn @ band-groupie.appspot.com"
    logging.info(flyer)
    
    #SEND EMAIL
    note = mail.EmailMessage()
    note.sender = 'concertReporter@band-groupie.appspotmail.com'
    note.to = email
    note.subject = 'Your Concerts Flyer from Band Groupie!'
    note.body = flyer
    note.send()


def getSimilar(SERVICE, b):  #b in query friendly string
    try:
        url = musicAPI[SERVICE]['u'] % (musicAPI[SERVICE]['k'], b)
        logging.info("fetching: %s"%url)
        response = urlfetch.fetch(url=url)
        return response
    except:
        logging.warning("API called failed: %s %s" % (SERVICE, b))
      

#DONE? -- check if suggested band is in user's fav list. Don't add if so
def connectSuggestedBand(original_band, suggested_band):

    logging.info("band:'%s' suggested:'%s'" % ( original_band, suggested_band ) )

    #TO BAND OBJECT
    band_obj = band.get_by_id(original_band.lower())
    if not (suggested_band in band_obj.similar_bands):
        band_obj.similar_bands.append(suggested_band)

    #TO USER OBJECT
    user_query = user.query(user.bands==original_band)
    if user_query is not None:
        for user_obj in user_query.fetch():
            if not (suggested_band in user_obj.bands) or not (suggested_band in user_obj.similar_bands):
                user_obj.similar_bands.append(suggested_band)
                user_obj.put()

    band_obj.put()
    
            
#---------------------------------------
# ADMIN DEBUG HANDLER(S)
#---------------------------------------
class SpitConcertList(webapp2.RequestHandler):
    def get(self):    
        
        concert_query = concert.query()
        if concert_query is not None:
            for concert_obj in concert_query.fetch():
                self.response.write("%s<br>"%concert_obj)


application = webapp2.WSGIApplication([
    ('/settings', UserSettings),
    ('/concerts', ConcertsInfo),
    ('/spitConcertList', SpitConcertList),
    ('/cron1', Scrapper),
    ('/cron2', Suggester),
    ('/cron3', Matcher),
    ('/cron4', Mailer),
    ('/', login),
], debug=True)
