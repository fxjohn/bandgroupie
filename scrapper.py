import os
import sys
import re
import datetime
import time
import lxml
import logging


#for bs4
sys.path.append(os.path.join(os.path.dirname(__file__), "lib")) 

from google.appengine.api import urlfetch
from bs4 import BeautifulSoup

CHRONICLE = "http://www.austinchronicle.com/gyrobase/m/Handler.html?Section=MusicListings&startTime="
C3CONCERTS1 ="http://www.c3concerts.com/rss-feed/"
C3CONCERTS2 ="http://www.c3concerts.com/concert-rss-feed/"
TICKETMASTER="http://m.ticketmaster.com/ticket/events.do?articles=tmus&region=40&days=0&category=1605"

def fetchWebsite(site):
    response = urlfetch.fetch(site)
    return response
#TODO --- CHECK response.code == 200 ... think of what to do if it isn't

#C3CONCERTS (gets all dates)
#returns data_c3c[e_id] = { door_time, event_date, event_title, ticket_link_raw, venue_name }
def c3c():
    stuff = fetchWebsite(C3CONCERTS1)
    sopa = BeautifulSoup(stuff.content, 'xml')
    #TEST code = open("c3c", 'r').read()
    #TEST sopa = BeautifulSoup(code, 'xml')
    
    grabber=re.compile("id|event_date|door_time|event_title|venue_name|raw")

    data_c3c = {}

    for e in sopa("event"):
        relevant = e.find_all(grabber) # e is single event
        #logging.info("%s"%relevant)
        e_dict = {}
        for field in relevant:
    
            tag = field.name
            value = field.text
        
            if re.search("event_id", tag):
                e_id = value
            else:
                e_dict[tag] = value
        
        #logging.info("%s : %s"%(e_id,e_dict))     
        data_c3c[e_id] = e_dict
    
    return data_c3c
                                     
#CHRONICLE (one day per grab)
#returns data[e_id] = [ 0=date, 1:(n-2)=artists, (n-1)=venue, n=time]  
def chronicle():
    today =  datetime.date.today()
    data = {}

    distance = re.compile(r"\d+(\.\d+)?miles")
    grabber=re.compile(r"\n|[;,]\s*|\s\s+")

    extra_string = "Outside:|Inside:|\xa0"
    extra = re.compile(extra_string)

    ignore_string  = "open\s*mic|the\s*brew|cd\s*release|Blue\s*mist|Hot Club of Cowtown|Chez Zee"
    ignore_string += "|Music4Music|Showcase|Sauce|Poetry Slam|Mic Exchange|Eurotrash|CDGB|karaoke"
    ignore_string += "|Hot Club of Cowtown|Mariachi Caballeros|Donn & the Station Masters|Z.Tejas"
    crap = re.compile(ignore_string, re.IGNORECASE)
    
    view_link = 'http://www.austinchronicle.com/gyrobase/m/Calendar/MusicListings?oid=' #+e_id

    for d in range(0,30): #Get next 30 days
        date = today + datetime.timedelta(days=d)
        date = date.strftime("%Y-%m-%d")
        time.sleep(1)
        
        stuff = fetchWebsite("%s%s"%(CHRONICLE,date))
        logging.info( "%s%s"%(CHRONICLE,date) )
        #logging.info( "%s"%(stuff.content) )
        sopa = BeautifulSoup(stuff.content)
        #TEST code = open("%s%s"%("ac",d), 'r').read()
        #TEST sopa = BeautifulSoup(code)
        
        e_id = '' 
        for e in sopa("li", class_="row"):
                        
            #get rid of crap        
            e_info = e.text
            if crap.search(e_info) : continue
            #logging.info("%s"% re.sub('\s+(\n)?', ' ', e_info))
        
            #only grab events with e_id and time
            if e.a and re.search(r":\d+[AaPp][Mm]", e_info): 
                fields = grabber.split(e_info)
                e_id = re.sub("\S+oid=", "", e.a.get('href'))
                #logging.info("%s"% re.sub('\s+(\n)?', ' ', e_info))
                if not e_id in data: 
                    data[e_id] = [ date ] #create list, date is first element
                    for field in fields:
                        if field: 
                            #get rid of distance
                            if  distance.search(field) : continue 
                            field = extra.sub("", field)                   
                            data[e_id].append(field)
            
                    #logging.info("%s : %s"%(e_id,data[e_id])) 
    
    return data
 
